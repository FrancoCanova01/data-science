# Data Science


## Spinger - Introduction to Data Science
To follow the books material do the following:
1. Clone the file into *LOCATION*
2. Run a Docker container that has jupyter notebook installed by mounting *LOCATION* as a volume. Eg: 
```bash
export LOCATION="C:\Users\canov\OneDrive\Documents\Obsidian Vaults\Remote Vaults\General\DataScience\DataScienceCodeSamples\Springer - Introduction to Data Science"

docker run -p 8888:8888 -v "$LOCATION:/home/jovyan/work" jupyter/scipy-notebook
```
3. Look for access url in container logs (it contains a tocken that will grant access to jupyter notebook session)
